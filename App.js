
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  FlatList,
  Dimensions,
  TouchableHighlight,
  Alert,
  TouchableOpacity,
  TouchableNativeFeedback
} from 'react-native'; 


// sample api http://droidtute.com/reactexample/sample_api/getMovieList.php

const { width, height } = Dimensions.get('window');

const equalWidth =  width*0.45;
const equalHeight =height*0.25;
export default class App extends Component {


  constructor(props) {
    super(props)
    this.state = {
      idList:[
        {'id':1,'scid':'SCMO_0002'},
        {'id':2,'scid':'SCMO_0003'},
        {'id':3,'scid':'SCMO_0004'},
        {'id':4,'scid':'SCMO_0005'},
        {'id':5,'scid':'SCMO_0006'},
        {'id':6,'scid':'SCMO_0007'},
        {'id':7,'scid':'SCMO_0008'},
        {'id':8,'scid':'SCMO_0009'},
     ],
      moviesList: []
    }
  }

  _keyExtractor = (item, index) => item.id;

  renderRowItem = (itemData) => {
    return (
      <View style={styles.thumbnail} >
      <TouchableNativeFeedback  background={TouchableNativeFeedback.Ripple('red')} 
 onPress={()=>{Alert.alert('https://www.smallcase.com/images/smallcases/187/'+itemData.item.scid+'.png')}}>
        <ImageBackground style={{   width : equalWidth,height:equalHeight ,  elevation:5

    
    }} source={{ uri: 'https://www.smallcase.com/images/smallcases/187/'+itemData.item.scid+'.png' }}   />
      </TouchableNativeFeedback>

      </View>
    )
  }

  componentWillMount() {
    {this.getMoviesFromApiAsync()}
  }


  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.idList}
          numColumns={2}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderRowItem}
        />
      </View>
    );
  }


  getMoviesFromApiAsync = () => {
    return fetch('http://droidtute.com/reactexample/sample_api/getMovieList.php')
      .then((response) => response.json())
      .then((responseJson) => {
       // alert(JSON.stringify(responseJson))
        this.setState({ moviesList: responseJson.movieList }) // this will update state to re-render ui
        return responseJson.movieList;
      })
      .catch((error) => {
        console.error(error);
      });
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d5d5d5',
    flexDirection: 'column'
  },
  thumbnail:{
    borderRadius:6,
    width:equalWidth,
    height:equalHeight,
    justifyContent:'center',
    elevation:5,
    overflow:'hidden',
    marginLeft:(width-(2*equalWidth))/3,
    marginBottom:height*0.02,

  }
});
 
import React, { Component } from 'react';
import { Text, Image, View, StyleSheet, ScrollView } from 'react-native';

class ScrollViewExample extends Component {
   state = {
      names: [
         {'name': 'Ben', 'id': 1},
         {'name': 'Susan', 'id': 2},
         {'name': 'Robert', 'id': 3},
         {'name': 'Mary', 'id': 4},
         {'name': 'Daniel', 'id': 5},
         {'name': 'Laura', 'id': 6},
         {'name': 'John', 'id': 7},
         {'name': 'Debra', 'id': 8},
         {'name': 'Aron', 'id': 9},
         {'name': 'Ann', 'id': 10},
         {'name': 'Steve', 'id': 11},
         {'name': 'Olivia', 'id': 12}
      ]
   }
   render() {
      return (
         <View>
            <ScrollView>
               {
                  this.state.names.map((item, index) ⇒ (
                     <View key = {item.id} style = {styles.item}>
                        <Text>{item.name}</Text>
                     </View>
                  ))
               }
            </ScrollView>
         </View>
      )
   }
}
export default ScrollViewExample

const styles = StyleSheet.create ({
   item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#d2f7f1'
   }
})