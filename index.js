import { AppRegistry } from 'react-native';
import App from './Application/index';

AppRegistry.registerComponent('smallcase', () => App);
