import {AsyncStorage,Alert,NetInfo} from 'react-native';
const smallcaseUrl='https://api-dev.smallcase.com/smallcases/smallcase?scid=';
const smallcaseHistoricalUrl='https://api-dev.smallcase.com/smallcases/historical?scid=';


export function fetchDetails(scid) {
    return (dispatch) => {

        var url = smallcaseUrl+scid;
        console.log('url is ',url);
        fetch(url,{method: 'GET' })   
        .then((response) => response.json())
        .then((responseJson) => {
        console.log('response is',responseJson)
        if(responseJson.success==true){
        //checking the success status so that if an api fails we can display the error message recieved by adding one else block    
        dispatch(fetchDetailsDataSuccess(responseJson,scid))
        }
        })
        .catch((error) => {
            console.log('error is in fetch data',error)
            dispatch(itemsHasErrored(true))

        }
        );
    };
}
export function fetchHistorical(scid) {
    return (dispatch) => {
            var url = smallcaseHistoricalUrl+scid;
            console.log('url is ',url);
            fetch(url,{method: 'GET' })   
            .then((response) => response.json())
            .then((responseJson) => {
            console.log('response is',responseJson)
            if(responseJson.success==true){
            //checking the success status so that if an api fails we can display the error message recieved by adding one else block    
            dispatch(fetchHistoricalDataSuccess(responseJson,scid))
            }
            })
            .catch((error) => {
                console.log('error is in fetch data',error)
                dispatch(itemsHasErrored(true))

            }
            );
    };
}

export function itemsHasErrored(bool) {
    console.log('error is dipatched')
    return {
        type: 'DATA_HAS_ERRORED',
        hasErrored: bool,
    };
}
//Havent Used below function ,if required can be uncommented
// export function itemsIsLoading(bool){
//     return {
//         type: 'DATA_IS_LOADING',
//         isLoading: bool,
//     };
// }
export function fetchDetailsDataSuccess(data,payload) {
    return {
        type: 'FETCH_DETAILS_SUCCESS',
        data,
        payload
    };
}
export function fetchHistoricalDataSuccess(data,payload) {
    return {
        type: 'FETCH_HISTORICAL_SUCCESS',
        data,
        payload
    };
}



 