import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../AppWithNavigation/AppWithNavigation';

const firstAction = AppNavigator.router.getActionForPathAndParams('Home');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
const initialNavState = AppNavigator.router.getStateForAction(
tempNavState);
  
export function navigationReducer(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    case 'Home':
    console.log('in home')
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.back(),
        state
      );
      break;
    case 'goToDetails':   
      nextState = AppNavigator.router.getStateForAction(
      NavigationActions.navigate({ routeName: 'Details'}),
      {...state,id:action.id}
      );
      break;  
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}

