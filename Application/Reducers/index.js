import { combineReducers,applyMiddleware,getState } from 'redux';
import {navigationReducer} from './navigationReducers';
import { fetchDetailsSuccess,internetStatus,fetchHistoricalSuccess, dataHasErrored } from './items';




const rootReducer= combineReducers({
  dataHasErrored,
  navigationReducer,
  
  fetchDetailsSuccess,
  fetchHistoricalSuccess
});
export default rootReducer;