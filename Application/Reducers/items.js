export function dataHasErrored(state = false, action) {
    switch (action.type) {
        case 'DATA_HAS_ERRORED':
            return action.hasErrored;

        default:
            return state;
    }
}
//Havent Used below reducer ,if required can be uncommented

// export function dataIsLoading(state = false, action) {
//     switch (action.type) {
//         case 'DATA_IS_LOADING':
//         console.log('in reducer odf loading',action.isLoading)
//             return action.isLoading;

//         default:
//             return state;
//     }
// }
// export function internetStatus(state = false, action) {
//     switch (action.type) {
//         case 'CHANGE_CONNECTION_STATUS':
//             return action.status;

//         default:
//             return state;
//     }
// }

const initialDetailsData={
    scid: [],
    response: { }
  }

export function fetchDetailsSuccess(state=initialDetailsData,action){
    switch (action.type) {
        case 'FETCH_DETAILS_SUCCESS':
        console.log('in fetchDetails Success');

        return {
            scid: [ ...state.scid, action.payload],
            response: {
            ...state.response,
            [action.payload]: action.data
          }
        }
      
        default:
            return state;
    }
}
const initialHistoricalData={
    scid: [],
    response: { }
  }

export function fetchHistoricalSuccess(state=initialDetailsData,action){
    switch (action.type) {
        case 'FETCH_HISTORICAL_SUCCESS':
        console.log('in fetchHistorcial Success');
        return {
            scid: [ ...state.scid, action.payload],
            response: {
            ...state.response,
            [action.payload]: action.data
          }
        }
      
        default:
            return state;
    }
}




