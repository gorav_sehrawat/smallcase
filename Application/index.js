
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react'
import AppWithNavigationState from './AppWithNavigation/AppWithNavigation';
import configureStore from './Store/configureStore';
const { persistor, store } = configureStore()

export default class App extends Component{
    
constructor(props){
    super(props);  
    this.state={ 
    }
}

render() {    
    return(
    <Provider store={store}>
    <PersistGate  persistor={persistor}>
    <AppWithNavigationState />
    </PersistGate>
    </Provider>    
)
}
}
