import React, { Component } from 'react';
import { Text, Image, View, StyleSheet,ToastAndroid,TouchableHighlight, ImageBackground,NetInfo, ActivityIndicator, BackHandler,Dimensions, ScrollView, Alert } from 'react-native';
import { connect } from 'react-redux';
import { VictoryLine,VictoryChart,VictoryVoronoiContainer,VictoryAxis,VictoryLabel } from 'victory-native';
import {fetchDetails,fetchHistorical} from '../Actions/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
var{height,width}=Dimensions.get('window');
const equalWidth =  width*0.65;
const equalHeight = height*0.33;

class Details extends Component {
    constructor(props){
        super(props); 
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);     
        this.state = {
        yearlyReturn:null,
        monthlyReturn:null,
        indexValue:null,
        status:true,

    }
}
handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
}
changeScroll(scrollEnabled) {
    this.setState({ scrollEnabled });
  }
handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });
  }
componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
}

componentWillMount(){
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.fetch().done(
        (isConnected) => { 
            isConnected?this.updateDate():null
            this.setState({status:isConnected})
            ;}
      );
 } 
//I have made this function - if in future we have to add try again functionality to fetch data ,then this can be used
updateDate(){
   
        if(this.props.detailsData[this.props.currentScId]==undefined ||this.props.historicalData[this.props.currentScId]==undefined){
            this.props.fetchData(this.props.currentScId);
            this.props.fetchHistorical(this.props.currentScId);
        } 
    
} 
graphData(){
    var data=[];
    //initializing data for graph
    this.props.historicalData[this.props.currentScId].data.points.map((item, index) => (
         data.push({x : item.date, y:item.index})
    ))
   
    return data;
}

render() {
    
    if(this.state.status==false){
        ToastAndroid.show('Please connect to the Internet', ToastAndroid.SHORT);

    }
    return (
    <View style={styles.container}>
    <View style={styles.titleBar}>
    <View style={{justifyContent:'center'}}>
    <TouchableHighlight underlayColor={'transparent'} onPress={()=>{this.handleBackButtonClick()}}>    
    <Icon name="chevron-left" size={36} color="#0486DB" />
    </TouchableHighlight>    
    </View>
    <View style={{justifyContent:'center'}}>
    <Text style= {styles.titleBarText}> {this.props.currentScId}</Text>
    </View>
    </View>
    <ScrollView>
    {
    this.props.detailsData[this.props.currentScId]==undefined ||this.props.historicalData[this.props.currentScId]==undefined?
    <View>    
    <ActivityIndicator size="large" color="#0486DB" />
    </View>
    :     
    <View style={styles.card}>
    <View style={styles.imageContainer}>
    <ImageBackground style={{   width : equalWidth,height:equalHeight ,borderRadius:10,  elevation:5}}
    source={{ uri: 'https://www.smallcase.com/images/smallcases/187/'+this.props.currentScId+'.png' }}   />
    </View>
    <View style={styles.detailContainer}>
    <Text style= {styles.titleBarText} >smallcase Details</Text>
    <Text > {this.props.currentScId}</Text>
    </View>
    <View style={styles.valueContainer}>
    <View style={styles.boxInsideValueContainer}>
    <Text style={styles.label} adjustsFontSizeToFit={true} numberOfLines={1}>Index Value</Text>
    <Text style={[styles.values, this.props.detailsData[this.props.currentScId].data.stats.indexValue&&styles.indexValue]}>
    {this.props.detailsData[this.props.currentScId].data.stats.indexValue.toFixed(2)}</Text>
    </View>    
    <View style={styles.boxInsideValueContainer}>
    <Text style={styles.label} adjustsFontSizeToFit={true} numberOfLines={1}>Yearly Return</Text>
    <Text style={[styles.values, this.props.detailsData[this.props.currentScId].data.stats.returns.yearly.toFixed(2)<0&&styles.lowValues]}>
    {this.props.detailsData[this.props.currentScId].data.stats.returns.yearly.toFixed(2)}%</Text>
    </View>    
    <View style={styles.boxInsideValueContainer}>
    <Text style={styles.label} adjustsFontSizeToFit={true} numberOfLines={1}>Monthly Return</Text>
    <Text style={[styles.values, this.props.detailsData[this.props.currentScId].data.stats.returns.monthly.toFixed(2)<0&&styles.lowValues]}>
    {this.props.detailsData[this.props.currentScId].data.stats.returns.monthly.toFixed(2)}%</Text>
    </View>    
    </View>
    <View style={styles.rationalContainer}>
    <Text  style={styles.rationalHeader}>Rationale</Text>
    <Text style={{color:'#696969'}} >{this.props.detailsData[this.props.currentScId].data.rationale}</Text>
    </View>
    <View style={styles.chart}>
    <Text style={{color:'#161C28',paddingLeft:width*0.03, fontSize:18, fontWeight:'bold'}}>Date vs. Index Value</Text>
    <VictoryChart
    domainPadding={2}
    height={height*0.45}
    width={width}
    padding={{ top:height*0.02,bottom:height*0.1,left:width*0.12,right:width*0.1,}}
    containerComponent={
    <VictoryVoronoiContainer
    onTouchStart={() => this.changeScroll(false)}
    onTouchEnd={() => this.changeScroll(true)}
    labels={(d) => `( ${d.x} , ${d.y} )`} />
    }
    >
    <VictoryLine 
    style={{ data: { stroke: "#0aaff1" }}} data={this.graphData()} />
    <VictoryAxis dependentAxis   />
    <VictoryAxis crossAxis style={{tickLabels: { fill: "none"}}}label={'Date(tap on the graph for detail)'}/>
    </VictoryChart>
    </View>
    </View>  
    }     
    </ScrollView>
    </View>
    )
   }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: '#f4f3f4',
        flexDirection: 'column'
    },
    titleBar:{
        backgroundColor:'#FEFFFC',
        height:height*0.08,
        elevation:5,
        flexDirection:'row'
      },
      titleBarText:{
        color:'#161C28',
        fontSize:18,
        fontWeight:'bold'
      },
  errorState:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  card:{
        flexDirection:'column',
    },
  imageContainer:{
        height:height*0.36,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        overflow:'hidden'
    },
    detailContainer:{
      flexDirection:'column',
      alignItems:'center' ,
      paddingBottom:height*0.02 
    },
  valueContainer:{
        height:height*0.08,
        backgroundColor:'white',
        width:width,
        alignSelf:'center',
        flexDirection:'row',
        elevation:1,

    },
  boxInsideValueContainer:{
        width:width*0.3,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginLeft:(width-(3*width*0.3))/4
    },
  rationalContainer:{
        flexDirection:'column',
        backgroundColor:'white',
        alignSelf:'center',
        width:width,
        paddingTop:width*0.01,
        paddingBottom:width*0.02,
        paddingLeft:width*0.03,
        marginTop:height*0.02,
        elevation:2,
    },
  rationalHeader:{
        fontSize:18,
        color:'#161C28',
        fontWeight:'bold',    
    },
  label:{
        color:'#0aaff1',
        fontSize:12,
    },
  values:{
        color:'green',
        fontWeight:'bold',
        elevation:5
    },
  lowValues:{
        color:'red'
    },
  indexValue:{
        color:'#161C28'
    },
  chart: {
        width:width,
        backgroundColor:'white',
        elevation:2,
        paddingTop:height*0.01,
        marginTop:height*0.02,
        marginBottom:height*0.02,
    }
      

})

  
const mapStateToProps = (state) => {
    return {
    currentScId:state.navigationReducer.id,
    detailsData:state.fetchDetailsSuccess.response,
    historicalData:state.fetchHistoricalSuccess.response,

    };
  };
  
const mapDispatchToProps = (dispatch) => {
    return {
    fetchHistorical: (scid) => dispatch(fetchHistorical(scid)),
    fetchData: (scid) => dispatch(fetchDetails(scid)),
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(Details);
  