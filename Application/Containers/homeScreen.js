
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {AppRegistry,StyleSheet,Text,View,StatusBar,ToastAndroid, ImageBackground,Image,NetInfo, FlatList,BackHandler,Platform, Dimensions,TouchableHighlight,Alert, TouchableOpacity,TouchableNativeFeedback, ScrollView} from 'react-native';
var{height,width}=Dimensions.get('window');
const equalWidth =  width*0.45;
const equalHeight = height*0.25;
const imageUrl='https://www.smallcase.com/images/smallcases/187/';
class HomeScreen extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      idList:[
        {'id':1,'scid':'SCMO_0002'},
        {'id':2,'scid':'SCMO_0003'},
        {'id':3,'scid':'SCMO_0009'},
        {'id':4,'scid':'SCMO_0010'},
        {'id':5,'scid':'SCMO_0011'},
        {'id':6,'scid':'SCMO_0007'},
        {'id':7,'scid':'SCMO_0008'},
        {'id':8,'scid':'SCMO_0009'},
        {'id':9,'scid':'SCMO_0001'},
        {'id':10,'scid':'SCMO_0005'},
        {'id':11,'scid':'SCMO_0006'},
        {'id':12,'scid':'SCMO_0003'},
        {'id':13,'scid':'SCMO_0004'},
        {'id':14,'scid':'SCMO_0005'},
        {'id':15,'scid':'SCMO_0007'},
        {'id':16,'scid':'SCMO_0008'},
        {'id':17,'scid':'SCMO_0004'},
        {'id':18,'scid':'SCMO_0005'},
        {'id':19,'scid':'SCMO_0011'},
     ],
     status:true,
     
    }
  }
  
componentWillUnmount() {
  //unmounting the event listeners
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);

}

handleBackButtonClick() {
  //to engage the user more , used this alert .
      Alert.alert(
        'Exit App',
        'Exiting the application?', [{
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
        }, {
            text: 'OK',
            onPress: () => BackHandler.exitApp()
        }, ], {
            cancelable: false
        }
      )
      return true;
}    
handleConnectionChange = (isConnected) => {
  //event handler to update the internet status
  this.setState({ status: isConnected });
}    
componentWillMount(){

      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
      NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
      NetInfo.isConnected.fetch().done(
        (isConnected) => { this.setState({ status: isConnected }); }
      );
}    

//function to navigate to the details screen
smallcaseDetails(itemData){
  this.props.navigation.dispatch({type:'goToDetails',id:itemData.item.scid})
}


//key extractor for flat list
_keyExtractor = (item, index) => item.id;
//function / can be dumb component also used to render the tiles
renderRowItem = (itemData) => {
    return (
      <View style={[styles.thumbnail, itemData.item.id==1 || itemData.item.id==2 ? { marginTop: height*0.02 } : { marginTop:0 }]} >
      <TouchableNativeFeedback  background={TouchableNativeFeedback.Ripple('#000000')} onPress={()=>{ this.smallcaseDetails(itemData)}}>
      <ImageBackground style={styles.imageStyle} source={{ uri: imageUrl+itemData.item.scid+'.png' }}   />
      </TouchableNativeFeedback>
      </View>
    )
}

render() {
  if(this.state.status==false ){
    //to show device status when it changes to offline mode
    ToastAndroid.show('Please connect to the Internet', ToastAndroid.SHORT);
  }
    return (
      <View style={styles.container}>
      <StatusBar  backgroundColor="#0486DB" barStyle="light-content" />
      <View style={styles.titleBar}>
      <View style={{alignItems:'center',justifyContent:'center',paddingLeft:width*0.03}}>
      <Image style={{height:height*0.06,width:height*0.06}} resizeMode={'contain'} source={require('../Resources/icon.jpg')}/>
      </View>
      <View style={{alignItems:'center',justifyContent:'center'}}>
      <Text style= {styles.titleBarText}> smallcase</Text>
      </View>
      </View>
      <FlatList
          data={this.state.idList}
          numColumns={2}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderRowItem}
          scrollEnabled = {true}
      />
      </View>
    );
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor: '#f4f3f4',
    flexDirection: 'column'
  },
  thumbnail:{
    borderRadius:4,
    width:equalWidth,
    height:equalHeight,
    justifyContent:'center',
    elevation:5,
    overflow:'hidden',
    marginLeft:(width-(2*equalWidth))/3,
    marginBottom:height*0.02,

  },
  titleBar:{
    backgroundColor:'#FEFFFC',
    height:height*0.08,
    elevation:5,
    flexDirection:'row'
  },
  titleBarText:{
    color:'#161C28',
    fontSize:20,
    fontWeight:'bold'
  },
  imageStyle:{
    width : equalWidth,
    height:equalHeight ,
    elevation:5
  }

});


const mapStateToProps = (state) => {
  return {
   
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
