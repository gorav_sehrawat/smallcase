import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, TabNavigator,StackNavigator,DrawerNavigator } from 'react-navigation';
import HomeScreen  from '../Containers/homeScreen'
import Details from '../Containers/detailsContainer'

export const AppNavigator = StackNavigator({
    Home: { screen: HomeScreen },
    Details:{screen:Details}
  },
{
 headerMode:'none', 
});
  

const AppWithNavigationState = ({ dispatch, nav }) => (
  
<AppNavigator 
    navigation={addNavigationHelpers({ dispatch, state: nav })} />
);
  
AppWithNavigationState.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
};
  
const mapStateToProps = state => ({
    nav: state.navigationReducer,    
});
  
export default connect(mapStateToProps)(AppWithNavigationState);
  
  
